from pprint import pprint

import numpy as np
import threading
import time
import logging

from PyTango import *
# import matplotlib.pyplot as plt
try:
    """for console"""
    from .comm import RgaTelnet
    from .analog_scan import AnalogScan
    from .peak_scan import PeakScan
    from .bar_scan import BarScan
    from .attributes import Attributes
    from .analog_bar_scan import AnalogBarScan

except:
    """for DS"""
    from comm import RgaTelnet
    from analog_scan import AnalogScan
    from peak_scan import PeakScan
    from attributes import Attributes
    from bar_scan import BarScan
    from analog_bar_scan import AnalogBarScan


class RGAController:
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s - %(name)s - %(threadName)s - '
                               ' %(levelname)s - %(message)s')

    def __init__(self, ip, port=10014):
        log_name = '{}Record'.format(__name__)
        self.log = logging.getLogger(log_name)

        self.port = port
        # self.ip = ip

        try:
            self.comm = RgaTelnet(ip, port)
        except:
            print("Device not available")






        #self.analog_scan = AnalogScan(self.comm)
        self.peak_scan = PeakScan(self.comm)
        self.peak_parameters = np.zeros(([1, 5]), dtype=int)

        self.scan = AnalogBarScan(self.comm)

        # self.scan=AnalogBarScan(self.comm)




        """testing read"""

        # read_tr = threading.Thread(target=self.read)
        # read_tr.start()

        """
        1On
        2Off
        3Close
        4Open
        5Insert
        6Extract
        7Moving
        8Standby
        9Fault
        10Init
        11Running
        12Alarm
        13Disable
        14Unknown
        """
        """runing mode"""
        # self.state = ['Running', 'On', 'StandBy', 'Fault']

        # self.n_detectors = self.get_n_detectors()

        # print("nº detectors= ", self.n_detectors)

        """default degass values"""
        #
        # self.degas_start_power = 30
        # self.degas_end_power = 80
        # self.degas_ramp_time = 60
        # self.degas_dwell_time = 60
        # self.degas_reset_time = 30
        #
        #
        # self.degas_start_power,
        # self.degas_end_power,
        # self.degas_ramp_time,
        # self.degas_dwell_time,
        # self.degas_reset_time  = 30,80,60,60,30
        #
        #
        # self.degas_settigs = 30,80,60,60,30
        #
        #
        # self.degas_settigs = {
        #     "degas_start_power": '30',
        #     "degas_end_power": '80',
        #     "degas_ramp_time": '90',
        #     "degas_dwell_time": '240',
        #     "degas_reset_time": '30'
        # }





        # self.degas_settigs.update()
        # print(self.degas_settigs.values())



        self.set_filament_off()
        self.filament_state = "OFF"
        # self.filament_selected = self.get_filament_number()
        # self.filament_selected = self.info["ActiveFilament"]

        # self.multiplier_protect()

        self.lock = threading.Lock()

        # self.gain_control()

        self.attributes = Attributes(self.comm)
        self.attributes.update_info_dict()
        self.attributes.update_source_info_dict()
        self.info = self.attributes.get_info()
        # self.comm.addFilamentSubscribers(self.get_filament_state())

        """analog scan settings"""



        self.start_mass = 2
        self.end_mass = 5
        self.points = 4
        self.accuracy = 2
        self.gain = 0
        self.source = 0
        self.detector = 0


        """to here"""

        self.analog_bar_settings = {
            'start_mass': '1',
            'end_mass': '2',
            'points_filter': '4',
            'accuracy': '2',
            'gain': '0',
            'source': '0',
            'detector': '0',
            'analog_bar': "analog"
        }

        self.analog_bar_settings.update()
        self.points_per_mass = '8'
        self.filter_mode = "PeakAverage"
        # 0 self.analog_pressures=np.array()
        self.working_mode = "analog"

        self.bar_scan = BarScan(self.analog_bar_settings)
        """peak scan settings"""

        # self.scan_parametersnp = np.zeros((10, 5), dtype=int)
        # self.peak_pressuresnp = np.zeros((10, 1), dtype=float)
        # self.peak_scan_meassures=[]
        # self.peak_pressures=[]

        # self.nScans = 10

        self.sensors = []
        self.update_sensors()
        self.gains = []
        self.update_gains()

        """CIRRUS"""
        self.cirrus = False

        a = self.send("CirrusInfo")
        if 'ERROR' in a:
            self.cirrus = False
        else:
            self.cirrus = True
        print("CIRRUS", self.cirrus)

    """main control"""

    def send(self, cmd):
        r = self.comm.send(cmd)
        # print(r)
        return r

    def read(self):
        r= self.comm.read()
        print("Read: ",r)
        return r

    def is_in_use(self):
        return self.send("SensorState")

    def gain_control(self):
        return self.send('Control "rgaDS" "0.1"')

    def release_control(self):
        return self.send("Release")

    """if the multiplier should be locked by software."""

    def multiplier_protect(self):
        return self.send("MultiplierProtect False")

    """detectors"""

    def update_sensors(self):
        r = self.send('DetectorInfo 0')


        a = r.split("\r\n")
        self.n_detectors = len(r[3:-2])

        for i in a[3:]:
            i = i.split(" ")
            if len(i) < 4:
                pass
            else:
                s = i[2] + i[4][0:4]
                self.sensors.append(s)

    def get_sensors(self):
        return self.sensors

    def update_gains(self):
        def removeZero(i):
            return i[
                   :i.index('.')]

        n = self.send("EGains")

        n = n.split("\r\n")
        for i in n:
            if len(i) > 12:
                self.gains.append(removeZero(i))
        else:
            pass

    def get_gains(self):
        return self.gains


    def get_diagnostic(self):
        return self.send('RunDiagnostics')

    def get_general_info(self):
        info=''
        for item in self.info:
            info += item + ' = ' + str(self.info[item]) +'\n'
        return info


    def get_max_mass(self):
        return int(self.info["MaxMass"])

    """ionizer / detector settings"""

    def get_electron_energy(self):
        return np.float(self.info['ElectronEnergy'])

    def set_electron_energy(self, value):
        if not 0 <= value <= 101:
            raise ValueError('Electron Energy must be between 0 and 100 eV')
        else:
            print(self.send('SourceElectronEnergy 0 ' + str(value)))

            self.info["EletronEnergy"] = value

    def get_ion_energy(self):
        return np.float(self.info['IonEnergy'])

    def set_ion_energy(self, value):
        if not 0 <= value <= 11:
            raise ValueError("Must be between 0 and 10 eV")
        else:
            str_to_send = ('SourceIonEnergy 0 ' + str(value))
            # print(self.send(str_to_send))
            self.info['IonEnergy'] = str(value)

    def get_extractor_voltage(self):
        return float(self.info['ExtractVolts'])

    def set_extraction_voltage(self, value):
        if not -130 <= value <= 1:
            raise ValueError('Extraction Voltage must be between -130 and 0 V')
        else:
            str_to_send = ('SourceExtract 0 ' + str(value))
            print(self.send(str_to_send))
            # self.update_source_info()
            self.info['ExtractVolts'] = str(value)

    def get_electron_emission(self):
        r = float(self.info['ElectronEmission'])
        return r

    def set_electron_emission(self, value):
        if not 0 <= value <= 6:
            print('Emission Current must be between 0 and 5 mA')
        else:
            self.send('SourceEmission 0 ' + str(value))
            self.info['ElectronEmission '] = str(value)

    def get_low_mass_alignment(self):
        return int(self.info['LowMassAlignment'])

    def set_low_mass_alignment(self, value):
        if not 1 <= value <= 65535:
            raise ValueError("Align value must be < 65535")
        else:
            self.send("SourceLowMassAlignment 0 " + str(value))
            self.info['LowMassAlignment'] = str(value)

    def get_high_mass_alignment(self):
        return int(self.info["HighMassAlignment"])

    def set_high_mass_alignment(self, value):
        if not 1 <= value <= 65535:
            raise ValueError("Align value must be < 65535")
        else:
            self.send("SourceHighMassAlignment 0 " + str(value))
            self.info["HighMassAlignment"] = str(value)

    def get_low_mass_resolution(self):
        return int(self.info['LowMassResolution'])

    def set_low_mass_resolution(self, value):
        if not 1 <= value <= 65535:
            raise ValueError(" value must be < 65535")
        else:
            self.send("SourceLowMassResolution 0 " + str(value))
            self.info['LowMassResolution'] = str(value)

    def get_high_mass_resolution(self):
        return int(self.info["HighMassAlignment"])

    def set_high_mass_resolution(self, value):
        if not 1 <= value <= 65535:
            raise ValueError(" value must be < 65535")
        else:
            self.send("SourceHighMassResolution 0 " + str(value))
            self.info['HighMassResolution'] = str(value)

    def get_max_pressure_recommended(self):
        maxPressure = float(self.info['MaxRecommendedPressure'])
        maxPressure = float("{:.2e}".format(maxPressure))
        return maxPressure

    """TODO check uility"""

    def get_n_faraday_gains(self):
        # return self.sourceInfo[9]
        return self.info['NumFaradayEGains']

    """TODO check uility"""

    def get_n_multiplier_gains(self):
        # return self.sourceInfo[10]
        return self.info['NumMultEGains']

    """filament management"""

    def get_filament_number(self):
        return int(self.info["ActiveFilament"])

    def get_filament_state(self):
        a = self.send("FilamentInfo")
        print('FilamentInfo: %s' % str(a))
        a = a[34:37]
        return a

    def set_filament2(self):
        self.send('FilamentSelect 2')
        self.info["ActiveFilament"] = 2

    def set_filament1(self):
        self.send('FilamentSelect 1')
        self.info["ActiveFilament"] = 1

    def set_filament_state(self, state):
        self.send('FilamentControl {a}'.format(a=state))

    """end to check"""

    def set_filament_off(self):
        self.send('FilamentControl Off')

    def set_filament_on(self):
        self.send('FilamentControl On')

    # def set_degas_start_power(self, value):
    #     # value = str(value)
    #     # self.degas_settigs["degas_start_power"] = value
    #     with self.lock:
    #         self.degas_start_power=value
    #
    # def get_degas_start_power(self):
    #     with self.lock:
    #         return self.degas_start_power
    #
    # def set_degas_end_power(self, value):
    #     # value = str(value)
    #     # self.degas_settigs["degas_end_power"] = value
    #     with self.lock:
    #         self.degas_end_power=value
    #
    # def get_degas_end_power(self):
    #      with self.lock:
    #         a=self.get_degas_end_power
    #         return a
    #
    #
    # def set_degas_ramp(self, value):
    #     # value = str(value)
    #     # self.degas_settigs["degas_ramp_time"] = value
    #
    #     with self.lock:
    #         self.degas_ramp_time=value
    #
    # def get_degas_ramp(self):
    #     with self.lock:
    #         self.degas_ramp_time
    #
    # def set_degas_dwell(self, value):
    #     # value = str(value)
    #     # self.degas_settigs["degas_dwell_time"] = value
    #      with self.lock:
    #          self.degas_dwell_time=value
    #
    # def get_degas_dwell(self):
    #     with self.lock:
    #         return self.degas_dwell_time
    #
    # def set_degas_reset(self, value):
    #     # value = str(value)
    #     # self.degas_settigs["degas_reset_time"] = value
    #     with self.lock:
    #          self.degas_reset_time=value
    #
    # def get_degas_reset(self):
    #     with self.lock:
    #         return self.degas_settigs["degas_reset_time"]
    #
    #
    # def get_degas(self):
    #     return self.send('DegasReading')

    # def set_degas_on(self, startPower, endPower, rampUP, rampDown, dwell):
    #         str= "StartDegas " + startPower+" "+ endPower+" "+ rampUP+\
    #              " "+ rampDown+" "+ dwell
    #         self.send(str)
    def set_degas_on(self, params):
            str= "StartDegas " + params
            self.send(str)


    def set_degas_stop(self):
        self.send('StopDegas')

    def degas_state(self):
        return self.send("FilamentInfo")


    """ analog scan"""

    # def set_start_mass(self, value):
    #     if not 1 <= value <= 101:
    #         raise ValueError("1st Mass must be between 1 ")
    #     else:
    #         self.analog_scan.analog_settings[0] = value
    #         self.bar_scan.bar_settings[0] = value

    def set_start_mass(self, value):
        if not 1 <= value <= int(self.info["MaxMass"]):

            raise ValueError("1st Mass must be between 1 ")
        else:
            self.analog_bar_settings["start_mass"] = value

    # def get_start_mass(self):
    #     with self.lock:
    #         return self.analog_scan.analog_settings[0]

    def get_start_mass(self):
        with self.lock:
            return int(self.analog_bar_settings["start_mass"])

    # def set_end_mass(self, value):
    #     with self.lock:
    #         self.analog_scan.analog_settings[1] = value
    #         self.bar_scan.bar_settings[1] = value
    #     self.analog_scan.do_lists()

    def set_end_mass(self, value):
        if int(self.analog_bar_settings["start_mass"]) <= value \
                <= int(self.info["MaxMass"]):
            # if value > int(self.analog_bar_settings["start_mass"]):
            with self.lock:
                self.analog_bar_settings["end_mass"] = str(value)
        else:
            print("end mass must be higher than start mass and lower than " \
                  "max allowed mas")

    # def get_end_mass(self):
    #     return self.analog_scan.analog_settings[1]

    def get_end_mass(self):
        with self.lock:
            return int(self.analog_bar_settings["end_mass"])

    # def set_points_per_mass(self, value):
    #     with self.lock:
    #         self.analog_scan.analog_settings[2] = value
    #     self.analog_scan.do_lists()

    def set_points_per_mass(self, value):
        with self.lock:
            self.points_per_mass = value
        # self.analog_scan.do_lists()

    def get_points_per_mass(self):
        with self.lock:
            return int(self.points_per_mass)

    # def set_accuracy(self, value):
    #     with self.lock:
    #         self.analog_scan.analog_settings[3] = value
    #         self.bar_scan.bar_settings[3] = value
    #     self.analog_scan.do_lists()

    def set_accuracy(self, value):
        if 9 > value > -1:
            with self.lock:
                self.analog_bar_settings['accuracy'] = str(value)
                return "accuracy new value {a}", value
        else:
            return "Accuracy must be between 0 and 8"
            # self.bar_scan.bar_settings[3] = value
        # self.analog_scan.do_lists()

    # def get_accuracy(self):
    #     with self.lock:
    #         return self.analog_scan.analog_settings[3]

    def get_accuracy(self):
        with self.lock:
            return int(self.analog_bar_settings['accuracy'])

    # def set_gain_in_use(self, value):
    #     if value not in len(self.gains):
    #         raise ValueError("gain index not available")
    #     else:
    #         self.analog_scan.analog_settings[4] = value
    #         self.bar_scan.bar_settings[4] = value
    #
    def set_gain_in_use(self, value):

        if value not in [int(x) for x in self.gains]:
            raise ValueError("gain not supported")
        else:
            with self.lock:
                self.analog_bar_settings['gain'] = str(value)

    #
    # def get_gain_in_use(self):
    #     with self.lock:
    #         return self.analog_scan.analog_settings[4]
    #
    def get_gain_in_use(self):
        with self.lock:
            # a = int(self.analog_bar_settings['gain'])
            # return int(self.gains[a + 1])
            #
            return int(self.analog_bar_settings['gain'])

    # def get_sensor_in_use(self):
    #     with self.lock:
    #         return self.analog_scan.analog_settings[6]

    def get_sensor_in_use(self):
        with self.lock:
            return int(self.analog_bar_settings['detector'])

    # def set_sensor_in_use(self, value):
    #     # if value not in np.arange(0, self.amount_detectors):
    #     #     raise ValueError("detector index must be between 0 and {a}".
    #     #                      format(a=self.amount_detectors))
    #     # else:
    #     with self.lock:
    #         self.analog_scan.analog_settings[6] = value

    def set_sensor_in_use(self, value):
        # if value not in np.arange(0, self.amount_detectors):
        #     raise ValueError("detector index must be between 0 and {a}".
        #                      format(a=self.amount_detectors))
        # else:
        with self.lock:
            self.analog_bar_settings['detector'] = value

    def set_bar_scan_start(self):
        self.working_mode = "bar"
        self.analog_bar_settings["analog_bar"] = "bar"
        self.analog_bar_settings["points_filter"] = "PeakAverage"
        self.scan.start_scan(self.analog_bar_settings)

    def set_analog_scan_start(self):
        self.working_mode = "analog"
        self.analog_bar_settings["analog_bar"] = "analog"
        self.analog_bar_settings["points_filter"] = self.points_per_mass
        self.scan.start_scan(self.analog_bar_settings)
        # return self.analog_scan.start_analog()

    """analog record loop"""

    # def get_analog_scan(self):
    #     return self.analog_scan.bunch_scan
    def set_analog_record_start(self, filename):
        self.scan.start_analog_record(filename)

    def set_analog_record_stop(self):
        self.scan.recording = False
        self.peak_scan.recording = False

    """peak jump mode"""

    # def peak_scan_add_mass_np(self, settings):
    #
    #     print("AA", type(settings))
    #     if self.nPeakMass < 10:
    #         # if self.peak_scan.availableScans <10:
    #
    #         a=(np.where(np.all([self.peak_scan.availableScans]==0, axis=1))) ##
    #
    #         settings = np.insert(settings, 3, 0)
    #         # settings.insert(3,0)
    #         settings = np.array(settings)
    #
    #         # TODEL self.scan_parametersnp[a[0][0]] =settings
    #
    #
    #         self.peak_scan.peak_parametersnp[self.peak_scan.availableScans] \
    #             = settings
    #
    #         self.peak_scan.peak_parametersnp[a[0][0]] = settings
    #
    #
    #         # self.send(strTosend)
    #         self.peak_scan.availableScans = self.peak_scan.availableScans + 1
    #
    #         # self.availableScans = self.availableScans + 1
    #         self.nPeakMass += 1
    #         print(self.peak_scan.peak_parametersnp)

    def peak_scan_add_mass_np(self, settings):
        """ mass, accuracy, gain, detector"""
        self.peak_scan.add_mass(settings)
        # settings = np.insert(settings, 3, 0)
        # settings=np.array([settings])
        # # settings = np.array([settings])
        # """ mass, accuracy, gain, detector"""
        # # settings=np.array([settings])
        # print("SETTT",settings)
        # if self.nScans > 0:

            # settings=np.array([settings])
            # settings.insert(3,0)

            # a=self.peak_scan.peak_parametersnp\
            #     (np.where(np.all([self.peak_scan.peak_parametersnp]==0,
            #                      axis=0))) ##

            # n=np.argmin(self.peak_scan.peak_parametersnp[:,0])
            # self.peak_scan.peak_parametersnp[n]=settings

        # self.peak_parameters = np.append(self.peak_parameters, settings,
        #                                  axis=0)
        #
        # self.peak_scan.peak_parametersnp = self.peak_parameters

            # self.peak_scan.peak_parametersnp = \
            #     np.append( self.peak_scan.peak_parametersnp, [[settings]], axis=0)
            # self.peak_scan.peak_parametersnp=a

            # self.send(strTosend)
            # self.peak_scan.availableScans -=1
            # self.availableScans = self.availableScans + 1
            # self.nScans -= 1


        # else:
        #     self.log.info('no space available for more masses to scan')
        #     print('no space available for more scans')
        #     # print(self.scan_parametersnp)
        #     print(self.peak_scan.peak_parametersnp)

    def peak_scan_remove_mass_by_index(self, peak):

        with self.lock:
            self.peak_scan.peak_parametersnp[peak] = [0, 0, 0, 0, 0]
            # self.nScans += 1

    def peak_scan_remove_mass_by_weight(self, mass):
        self.peak_scan.remove_mass(mass)

    def peak_scan_remove_all(self):
        self.peak_scan.remove_all()

    def get_peak_scan_gains(self):
        with self.lock:
            return self.peak_scan.get_gains()

    def get_peak_scan_settings(self):
        with self.lock:
            return self.peak_scan.scan_settings

    # def set_peak_record_start(self, filename):
    #     self.peak_scan.start_peak_record(filename)

    def set_peak_scan_start(self):
        self.working_mode = "peak"
        return self.peak_scan.start_peak_scan()

    # def get_peak_pressures(self):
    #     with self.lock:
    #         return self.peak_scan.peak_pressures

    def get_peak_scan_detectors(self):
        with self.lock:
            return self.peak_scan.get_detectors()

    def get_peak_scan_accuracies(self):
        with self.lock:
            return self.peak_scan.get_accuracies()

    # def get_peak_scan_masses(self):
    #     with self.lock:
    #         a = self.peak_scan.get_masses()
    #         print("MASSES TO SCAN=", a)
    #         return a

    def get_peak_scan_massesnp(self):
        with self.lock:
            a = self.peak_scan.peak_parametersnp[:, 0]
            print("MASSES TO SCAN=", a)
            return a

    # def get_peak_scan_pressures(self):
    #     with self.lock:
    #         return self.peak_scan.peak_pressuresnp

    """ get scan result"""

    # def get_pressures(self):
    #     if self.working_mode == "analog":
    #         with self.lock:
    #             b = self.analog_scan.get_pressures()
    #             return b
    #     if self.working_mode == "bar":
    #         pass
    #         # with self.lock:
    # b = self.      # return self.peak_pressuresnp
    # plt.plot(self.peak_scan.peak_pressuresnp)
    # plt.show()
    # return self.peak_scan.peak_pressuresnp

    # TODO check utility
    def get_peak_scan_measure(self):
        with self.lock:
            return self.peak_scan.get_full_scan()

    """ get scan result"""

    # def get_pressures(self):
    #     if self.working_mode == "analog":
    #         with self.lock:
    #             b = self.analog_scan.get_pressures()
    #             return b
    #     if self.working_mode == "bar":
    #         with self.lock:
    #             b = self.bar_scan.get_pressures()
    #             return b
    #     if self.working_mode == "peak":
    #         with self.lock:
    #             b = self.peak_scan.get_pressures()
    #             return b

    def get_peak_pressures(self):
        # print("Working Mode", self.working_mode)
        # a = self.working_mode
        # if a == "analog" or a == "bar":
        #     print("AA")
        #     with self.lock:
        #         return self.scan.get_pressures()
        # else:
        #     if self.working_mode == "peak":
        with self.lock:
            return self.peak_scan.get_pressures()




    def get_analog_full_scan(self):
        with self.lock:
            return self.scan.get_analog_masses_pressures_scan()

    def get_analog_pressures(self):
        with self.lock:
            return self.scan.get_real_time_pressures()

    def get_analog_masses(self):
        with self.lock:
            return self.scan.get_masses()

    def get_time_scan(self):
        with self.lock:
            return self.scan.time_scan()

    # def get_masses(self):
    #     if self.working_mode == "analog":
    #         with self.lock:
    #             b = self.analog_scan.get_masses()
    #             return b
    #     if self.working_mode == "bar":
    #         with self.lock:
    #             b = self.bar_scan.get_masses()
    #             print(b)
    #             return b
    #     if self.working_mode == "peak":
    #         with self.lock:
    #             b = self.peak_scan.get_masses()
    #             print(b)
    #             return b

    # def get_masses(self):
    #     print("Working MOde", self.working_mode)
    #     a = self.working_mode
    #     if a == "analog" or a == "bar":
    #         with self.lock:
    #             return self.scan.get_masses()
    #     else:
    #         if self.working_mode == "peak":
    #             with self.lock:
    #                 return self.peak_scan.get_masses()

    """ stop scan independently mode"""

    # def stop_scan(self):
    #     if self.working_mode == "analog":
    #         with self.lock:
    #             self.analog_scan.analog_scan_stop.set()
    #
    #     if self.working_mode == "bar":
    #         with self.lock:
    #             self.bar_scan.bar_scan_stop.set()
    #
    #     if self.working_mode == "peak":
    #         with self.lock:
    #             self.peak_scan.peak_scan_stop.set()
    def stop_scan(self):

        if self.working_mode == "analog" or self.working_mode == "bar":
            # with self.lock:
            #     self.scan.scan_th_stop.set()

            self.scan.stopScan()


        # if self.working_mode == "bar":
        #     with self.lock:
        #         self.scan.scan_th_stop.set()

        if self.working_mode == "peak":
            # with self.lock:
            #     self.peak_scan.peak_scan_stop.set()
            self.peak_scan.stopScan()

    """CIRRUS"""

    def cirrusInfo(self):
        if self.cirrus:
            return self.send("CirrusInfo")
        else:
            return "not available for this model"

    def cirrusHeater(self, state):
        if self.cirrus:
            strToSend = "CirrusHeater " + str(state)
            return self.send(strToSend)
        else:
            return "not available for this model"

    def cirrusPump(self, state):
        if self.cirrus:
            strToSend = "CirrusPump " + str(state)
            return self.send(strToSend)
        else:
            return "not available for this model"

    def cirrusCapillaryHeater(self, state):
        if self.cirrus:
            strToSend = "CirrusCapillaryHeater " + str(state)
            return self.send(strToSend)
        else:
            return "not available for this model"

    def cirrusValvePosition(self, state):
        if self.cirrus:
            strToSend = "CirrusValvePosition " + str(state)
            return self.send(strToSend)
        else:
            return "not available for this model"


if __name__ == "__main__":
    r = RGAController('192.168.1.200')
    print("====================")
    print(r.set_filament_on())
    time.sleep(1)
    print(r.read())
    time.sleep(1)
    print(r.read())
    time.sleep(1)
    print(r.read())
    print("====================")
    print(r.set_degas_on,["10", "60", "20","60","20","20"])
    a=1
    while a<200:
        print("====================")
        print(r.read())
        time.sleep(1)
        a+=1
    print(r.set_filament_off())

    # r.set_degas_on()
    #ff
    # while True:
    #     print(r.read())
    #     time.sleep(1)


    # print("SENSORS")
    # print(r.sensors)
    #
    # print(r.gains)
    # r.set_start_mass(4)
    # r.set_detector(2)
    # r.set_analog_scan_start()
    # time.sleep(2)
    # r.set_analog_record_start('/home/lluis/Escritorio/RGAs/testFile/')
